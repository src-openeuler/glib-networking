Name:           glib-networking
Version:        2.78.0
Release:        1
Summary:        Network-related modules for glib
License:        LGPLv2+
URL:            https://gitlab.gnome.org/GNOME/glib-networking
Source0:        https://download.gnome.org/sources/glib-networking/2.78/%{name}-%{version}.tar.xz

BuildRequires:  meson gcc ca-certificates gettext systemd
BuildRequires:  pkgconfig(glib-2.0) >= 2.73.3 pkgconfig(gnutls) >= 3.7.4
BuildRequires:  pkgconfig(gio-2.0) pkgconfig(gsettings-desktop-schemas)
BuildRequires:  pkgconfig(libproxy-1.0) pkgconfig(p11-kit-1)

Requires:       ca-certificates gsettings-desktop-schemas glib2 >= 2.73.3
Recommends:     libproxy-duktape

%description
glib-networking contains the implementations of certain GLib networking features
that cannot be implemented directly in GLib itself because of their dependencies.

%package tests
Summary:   verify the Usability of the glib-networking package
Requires:  %{name} = %{version}-%{release}

%description tests
verify the Usability of the glib-networking package.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -Dinstalled_tests=true
%meson_build

%install
%meson_install

%find_lang %{name}

%check
%meson_test

%files -f %{name}.lang
%defattr(-,root,root)
%license COPYING
%doc NEWS README
%{_libdir}/gio/modules/libgiognomeproxy.so
%{_libdir}/gio/modules/libgiognutls.so
%{_libdir}/gio/modules/libgiolibproxy.so
%{_libexecdir}/glib-pacrunner
%{_datadir}/dbus-1/services/org.gtk.GLib.PACRunner.service
%{_userunitdir}/glib-pacrunner.service

%files tests
%defattr(-,root,root)
%{_libexecdir}/installed-tests/glib-networking
%{_datadir}/installed-tests

%changelog
* Mon Feb 5 2024 yanglu <yanglu72@h-partners.com> - 2.78.0-1
- update glib-networking version to 2.78.0:
  - disable PKCS#11 tests when GNuTLS is built without PKCS#11 support
  - fix connection tests on 32-bit systems with 64-bit time_t
  - updated translations

* Tue Jan 16 2024 yanglu <yanglu72@h-partners.com> - 2.76.1-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build error

* Tue Sep 26 2023 yanglu <yanglu72@h-partners.com> - 2.76.1-1
- update version to 2.76.1 and enable test

* Wed Aug 9 2023 zhangpan <zhangpan@h-partners.com> - 2.74.0-2
- disable make check

* Mon Jan 2 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 2.74.0-1
- Update to 2.74.0

* Thu Feb 02 2023 yangchenguang <yangchenguang@uniontech.com> - 2.72.0-2
- tests skip tls-exporter test for TLS 1.2

* Mon May 30 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 2.72.0-1
- Update to 2.72.0

* Mon Apr 25 2022 yanglu <yanglu72@h-partners.com> - 2.68.1-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix source0

* Fri Jan 07 2022 xingwei <xingwei14@huawei.com> - 2.68.1-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:allow tls-unique channel binding test to fail

* Fri Dec 03 2021 xihaochen <xihaochen@huawei.com> - 2.68.1-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update glib-networking to 2.68.1

* Mon Jul 19 2021 lijingyuan <lijingyuan3@huawei.com> - 2.62.4-2
- Type:requirement
- ID:NA
- SUG:NA
- DESC:cancel gdb in buildrequires

* Mon May 24 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 2.66.0-1
- Upgrade to 2.66.0
- Update Version, Source0, BuildRequires, Requires

* Sat Jul 25 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.62.4-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to 2.62.4

* Fri Mar 20 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.58.0-7
- add gdb in buildrequires

* Mon Mar 16 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.58.0-6
- sync handle new gnutls certificate requred patch

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.58.0-5
- clean unused patch

* Tue Dec 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.58.0-4
- change the path of files

* Tue Sep 17 2019 Lijin Yang <yanglijin@huawei.com> - 2.58.0-3
- Package init
